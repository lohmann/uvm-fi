### UVM-SystemC Fault Injection
Fault Injection Universal Verification Methodology (UVM-FI) in SystemC.
- (C) Copyright 2017,2018 Douglas Lohmann <<dlohmann0@gmail.com>>.

### What is UVM-SystemC Fault Injection? 

UVM-SystemC Fault Injection is an extend Universal Verification Methodology (UVM) that allows the injection of failures in the SystemC DUT. This implementation is  developed by Douglas Lohmann, it is based on [uvm-systemc-1.0-alpha1](http://www.accellera.org/downloads/drafts-review) version developed by Accelerra verification group. 

##### What kind of faults can I inject?
* Using UVM-SystemC Fault Injection tool you are able to inject the following faults:
    * ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `In development`.


### Running on Docker

Build:
```sh
    $ docker build . --no-cache
```

Start docker with your container tag:
```sh
    $ docker run -it -v "$PWD":/home/dummy 876b00058d0f zsh
```

```sh
    SC_SIGNAL_WRITE_CHECK=DISABLE ./build/examples/matrix/testbench/matrix
```

### How to install ![#1589F0](https://placehold.it/15/1589F0/000000?text=+)

In `uvm-fi/sources` you have the necessary files to install and use the extended UVM.

Before install uvm-fi you need some requirement to install and use UVM-SystemC Fault Injection: 

|     Tool         |   Version  | Tested version  |      Download     |
| ---------------- | ---------- | --------------- | ----------------- |  
| GNU C++ compiler |   4.4.7 >  |     4.9.2       | https://gcc.gnu.org/ |
| GNU Make (gmake) |   4.0 >    |     4.0         | https://www.gnu.org/software/make/ |
| SystemC          |   2.3.0 >  |     2.3.1       | http://www.accellera.org/downloads/standards/systemc |
+ UVM-SystemC Fault Injection was tested on the following platforms:
    + Debian GNU/Linux 8.6 (jessie);
    + Windows was `not` tested.

To install UVM-SystemC Fault Injection follow the Installation notes for UVM-SystemC discribed in `uvm-fi/sources/INSTALL` file.

In `uvm-fi/sources/examples` directory there are several examples that can be tested. 

### How to use
Instructions to use the UVM-FI library.
 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `In development`.

### For Developers ![#1589F0](https://placehold.it/15/1589F0/000000?text=+)
To facilitate the development, some scripts have been developed for library compilation. These scripts are in the `uvm-fi/scripts` directory.

These scripts will compile the UVM-FI and create a ready-to-use version in the `uvm-fi/uvm_gen` directory. When there is a change in the UVM-FI just run the scripts again. For more details about the settings refer to the README file in `uvm-fi/scripts` diretory.

### Project Organization ![#1589F0](https://placehold.it/15/1589F0/000000?text=+) 
This section describe the project organization in git and the internal directories. 

##### GIT 

This project has a git branching model using a `master` (stable version), and a `develop` (the current developing version) with main branches. When a bug is detected in a master version a new branch called `hotfixes` is open to fix the bug and then merge to master. 

New features are implemented with new branches from develop version. When we have a stable version of develop branch we create a `test` branch to make tests until merge to master.

In sumary we have the following branches:
* `master` (stable version).
    * `develop` (developing).
        * `new features` branches (add new features).
        * `test` (test version until merge to master).
    * `hotfixes` (Urgent bug fix in stable version).


It is recommended that new developers read the following websites before beginning the contribution:
* [Understanding the GitHub Flow](https://guides.github.com/introduction/flow/);
* [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/).

##### Internal directories

This repository is organized according to the following structure:

* [uvm-fi]() - Root diretory.
    * [examples](./examples) - Test examples.
        * [scoreboard](./examples/scoreboard) - ![#c5f015](https://placehold.it/15/f03c15/000000?text=+)
    * [sources](./sources) - UVM-FI code implementation.
        * [config](./sources/config) - Configurations scripts.
        * [docs](./sources/docs) - UVM-SystemC documentation.
        * [examples](./sources/examples) - UVM-SystemC examples (no FI).
        * [src](./sources/src) - UVM-FI source code.
        * [INSTALL](./sources/INSTALL) - Installations instructions.
    * [scripts](./scripts) - Scripts used to generate the compiled library.
        * [gen.sh](./scripts/gen.sh) - Compile and generate the new library.
        * [setSC](./scripts/setSC) - Set the environment.
        * [README](./scripts/README) - Instructions to run the scripts.
    * [README.md](./README.md) - This tutorial.
    * [LICENSE](./LICENSE) - Project License.

* When you run the scripts a new diretory will be create in `uvm-fi/uvm_gen`. This repository will contain a compiled UVM-FI library with the current changes in source files. 
* This three is not automatic generated, so the three is not complete and you should check on project diretory for more details.

### License and Maintainers 
Licenses:
 - The UVM-SystemC library is licensed under the Apache-2.0 license.The Full text of the license is provided in this kit in the file LICENSE. For more details go to [Accellera web page](http://www.accellera.org/downloads/drafts-review).
 - The extend UVM-SystemC Fault Injection is licensed under the Apache-2.0 license. The full text of the license is provided in this kit in the file LICENSE.
 
 Maintainers:
 - This implementation is maintained by Douglas Lohmann. Any questions or suggestions send him an email: <dlohmann0@gmail.com>.

--------------------
##### LABELS
+ ![#1589F0](https://placehold.it/15/1589F0/000000?text=+) - Tested.
+ ![#c5f015](https://placehold.it/15/f03c15/000000?text=+) - Developing.
+ ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) - Not tested.

> Douglas Lohmann - 05/16/2017 





