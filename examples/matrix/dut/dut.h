#ifndef DUT_H_
#define DUT_H_

#include <systemc.h>

#include "matrix_mult.h"
#include "printer.h"

class Dut : public sc_core::sc_module {
 public:
  sc_core::sc_in<sc_dt::sc_int<16> > dut_input_a[16];
  sc_core::sc_in<sc_dt::sc_int<16> > dut_input_b[16];
  sc_core::sc_port<sc_core::sc_signal_inout_if<sc_dt::sc_int<16> >, 2> dut_output_a[16];

  sc_core::sc_signal<sc_dt::sc_int<16>> signal_connection[16];

  MatrixMultiplier matrix_multiplier_;
  Printer printer_;

  SC_CTOR(Dut) : matrix_multiplier_("multiplier"), printer_("printer") {
    for (int z=0; z<16; z++) {
      matrix_multiplier_.input_a[z](dut_input_a[z]);
      matrix_multiplier_.input_b[z](dut_input_b[z]);
      matrix_multiplier_.output_a[z](signal_connection[z]);
      printer_.input_a[z](signal_connection[z]);
      printer_.output_a[z](dut_output_a[z]);
    }
  }
};

#endif /* DUT_H_ */
