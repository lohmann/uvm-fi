#ifndef PRINTER_H_
#define PRINTER_H_

#include <systemc.h>

class Printer : public sc_core::sc_module {
 public:
  sc_core::sc_in<sc_dt::sc_int<16> > input_a[16];
  sc_core::sc_port<sc_core::sc_signal_inout_if<sc_dt::sc_int<16> >, 2> output_a[16];

  void print();

  SC_CTOR(Printer) {
    SC_METHOD(print);for(int k=0;k<16;k++) {
          sensitive << input_a[k];
          dont_initialize();
    }
    }
};

#endif /* PRINTER_H_ */
