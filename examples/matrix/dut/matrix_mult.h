#ifndef MATRIXMULTIPLIER_H_
#define MATRIXMULTIPLIER_H_

#include <systemc.h>

class MatrixMultiplier : public sc_core::sc_module {
 public:
  sc_core::sc_in<sc_dt::sc_int<16> > input_a[16];
  sc_core::sc_in<sc_dt::sc_int<16> > input_b[16];
  sc_core::sc_out<sc_dt::sc_int<16> > output_a[16];

  int i, j;

  void mult();

  SC_CTOR(MatrixMultiplier) {
    SC_METHOD(mult);for(int k=0;k<16;k++) {
          sensitive << input_a[k] << input_b[k];
          dont_initialize();
    }
    }
};

#endif /* MATRIXMULTIPLIER_H_ */
