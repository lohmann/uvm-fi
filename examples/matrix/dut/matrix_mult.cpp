#include "matrix_mult.h"

void MatrixMultiplier::mult() {
  for (i = 0; i < 16; i = i + 4) {
    for (j = 0; j < 4; j++) {
      output_a[i + j]->write(
          input_a[0 + i].read() * input_b[j].read()
              + input_a[1 + i].read() * input_b[4 + j].read()
              + input_a[2 + i].read() * input_b[8 + j].read()
              + input_a[3 + i].read() * input_b[12 + j].read());
    }
  }
}
