#include <systemc>
#include <uvm>

#include <iostream>

#include "vip_if.h"
#include "testbench.h"
#include "test.h"
#include <dut.h>

int sc_main(int, char*[])
{
  Dut* my_dut = new Dut("my_dut");

  vip_if* dut_if_in_A = new vip_if();
  vip_if* dut_if_in_B = new vip_if();
  vip_if* dut_if_out = new vip_if();

  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vifA", dut_if_in_A);
  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vifB", dut_if_in_B);
  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vifOut", dut_if_out);

  for (int z=0; z<16; z++) {
      my_dut->dut_input_a[z](dut_if_in_A->sig_data[z]);
      my_dut->dut_input_b[z](dut_if_in_B->sig_data[z]);
      my_dut->dut_output_a[z](dut_if_out->sig_data[z]);
  }

  // fault trigger (When I enter with matrix b)
  sc_core::sc_in<sc_dt::sc_int<16> > *a = &(my_dut->matrix_multiplier_.input_b[15]);
  uvm::uvm_config_db<sc_core::sc_in<sc_dt::sc_int<16> > **>::set(0, "*", "input_event", &a);

//  sc_core::sc_signal<sc_dt::sc_int<16> > *b = &(dut_if_in_B->sig_data[15]);
//  uvm::uvm_config_db<sc_core::sc_signal<sc_dt::sc_int<16> > **>::set(0, "*", "dut_internal_signal", &b);

  // locale that faults will be injected (I will inject a fault in output matrix value)
  sc_core::sc_signal<sc_dt::sc_int<16> > *b = &(my_dut->signal_connection[14]);
  uvm::uvm_config_db<sc_core::sc_signal<sc_dt::sc_int<16> > **>::set(0, "*", "dut_internal_signal", &b);

  uvm::run_test("test");

  return 0;
}
