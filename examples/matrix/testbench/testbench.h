#ifndef TESTBENCH_H_
#define TESTBENCH_H_

#include <systemc>
#include <tlm.h>
#include <uvm>

#include "vip_agent.h"
#include "vip_packet.h"
#include "scoreboard.h"
#include "sequence.h"

#include "vip_fault_env.h"

class testbench : public uvm::uvm_env
{
 public:
  // instances
  vip_agent* agent_out;
  vip_agent* agent_A;


  scoreboard* scoreboard0;

  vip_fault_env* fault_env;

  UVM_COMPONENT_UTILS(testbench);

  testbench( uvm::uvm_component_name name) : uvm::uvm_env(name)
  {
   std::cout << sc_core::sc_time_stamp() << ": constructor " << name << std::endl;
  }

  void build_phase(uvm::uvm_phase& phase)
  {
    std::cout << sc_core::sc_time_stamp() << ": build_phase " << name() << std::endl;

    uvm::uvm_env::build_phase(phase);

    agent_out = vip_agent::type_id::create("agent_out", this);
    assert(agent_out);

    agent_A = vip_agent::type_id::create("agent_A", this);
    assert(agent_A);


    scoreboard0 = scoreboard::type_id::create("scoreboard0", this);
    assert(scoreboard0);

    fault_env = vip_fault_env::type_id::create("fault_env", this);
    assert(fault_env);

    uvm::uvm_config_db<int>::set(this, "agent_A", "is_active", uvm::UVM_ACTIVE);
    uvm::uvm_config_db<int>::set(this, "agent_out", "is_active", uvm::UVM_PASSIVE);

    uvm::uvm_config_db<uvm_object_wrapper*>
      ::set(this,"agent_A.sequencer.run_phase","default_sequence",
      sequence<vip_packet>::type_id::get());

  }

  void connect_phase(uvm::uvm_phase& phase)
  {
    std::cout << sc_core::sc_time_stamp() << ": connect_phase " << name() << std::endl;

    agent_A->monitor_in->item_collected_port.connect(scoreboard0->xmt_listener_imp);
    agent_out->monitor_out->item_collected_port.connect(scoreboard0->rcv_listener_imp);
  }

};

#endif /* TESTBENCH_H_ */
