#ifndef SCOREBOARD_H_
#define SCOREBOARD_H_

#include <systemc>
#include <uvm>
#include <vector>

#include "xmt_subscriber.h"
#include "rcv_subscriber.h"
#include "vip_packet.h"

class scoreboard : public uvm::uvm_scoreboard
{
 public:
  uvm::uvm_analysis_export<vip_packet> xmt_listener_imp;
  uvm::uvm_analysis_export<vip_packet> rcv_listener_imp;

  xmt_subscriber* xmt_listener;
  rcv_subscriber* rcv_listener;

  std::vector<int> mistakes; // number of erros per matrix

  sc_dt::sc_int<16> result[16];

  scoreboard( uvm::uvm_component_name name ) : uvm::uvm_scoreboard( name )
  {
    std::cout << sc_core::sc_time_stamp() << ": constructor " << name << std::endl;
  }

  UVM_COMPONENT_UTILS(scoreboard);

  void matrix_mult(const vip_packet& p){
    for(int i=0; i < 16; i=i+4){
      for (int j=0; j<4; j++){
      result[i+j] = p.a[0+i]*p.b[j] + p.a[1+i]*p.b[4+j] + p.a[2+i]*p.b[8+j] + p.a[3+i]*p.b[12+j];
      }
    }
  }  


  void write_xmt(const vip_packet& p)
  {
    std::cout << sc_core::sc_time_stamp() << ": " << name() << " xmt_listener in scoreboard received value A " <<  std::endl;
    for(int i=0; i<16; i = i+4) {
      std::cout << " " << p.a[i] << ", " << p.a[i+1] << " ," << p.a[i+2] << " ," << p.a[i+3] <<'\n';
    }

    std::cout << sc_core::sc_time_stamp() << ": " << name() << " xmt_listener in scoreboard received value B " <<  std::endl;
    for(int j=0; j<16; j = j+4) {
      std::cout << " " << p.b[j] << ", " << p.b[j+1] << " ," << p.b[j+2] << " ," << p.b[j+3] <<'\n';
    }

    matrix_mult(p);

  }

  void write_rcv(const vip_packet& p)
  {
    std::cout << sc_core::sc_time_stamp() << ": " << name() << " rcv_listener in scoreboard received value " << std::endl;
    for(int j=0; j<16; j = j+4) {
      std::cout << " " << p.b[j] << ", " << p.b[j+1] << " ," << p.b[j+2] << " ," << p.b[j+3] <<'\n';
    }

    mistakes.push_back(0);
    for(int i=0; i<16; i++){
      if(p.b[i] != result[i])
        mistakes.back()++; //= mistakes.at(i)+1;
    }
    
  }

  void build_phase(uvm::uvm_phase& phase)
  {
    uvm::uvm_scoreboard::build_phase(phase);
    std::cout << sc_core::sc_time_stamp() << ": build_phase " << name() << std::endl;

    uvm::uvm_config_db<uvm_object*>::set(this, "xmt_listener", "sb", this);
    uvm::uvm_config_db<uvm_object*>::set(this, "rcv_listener", "sb", this);

    xmt_listener = xmt_subscriber::type_id::create("xmt_listener", this);
    assert(xmt_listener);

    rcv_listener = rcv_subscriber::type_id::create("rcv_listener", this);
    assert(rcv_listener);
  }

  void connect_phase(uvm::uvm_phase& phase)
  {
    xmt_listener_imp.connect(xmt_listener->analysis_export);
    rcv_listener_imp.connect(rcv_listener->analysis_export);
  }

};

#endif /* SCOREBOARD_H_ */
