//----------------------------------------------------------------------
//   Copyright 2012-2014 NXP B.V.
//   All Rights Reserved Worldwide
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//----------------------------------------------------------------------

#ifndef VIP_FAULT_ENV_H_
#define VIP_FAULT_ENV_H_

#include <systemc>
#include <tlm.h>
#include <uvm>

#include "fi_lib/uvm_visitor.h"
#include "fi_lib/uvm_var_watcher.h"
#include "fi_lib/uvm_eca_engine.h"

#include "fi_lib/uvm_expr/uvm_expr.h"
#include "fi_lib/uvm_expr/uvm_var_tpl.h"

#include "fi_lib/uvm_expr/uvm_default_event.h"

#include "fi_lib/uvm_faults/uvm_fault.h"
#include "fi_lib/uvm_faults/uvm_fault_set.h"

#include "fi_lib/uvm_base_fault_env.h"

#include "fi_lib/uvm_dsl.h"

class vip_fault_env : public uvm_base_fault_env
{
 public:

  vip_fault_env( uvm::uvm_component_name name ) : uvm_base_fault_env( name ){
    std::cout << sc_core::sc_time_stamp() << ": constructor " << name << std::endl;
  }

  void dsl_examples() {
    using namespace uvm_dsl;
    uvm_expr* x1 = (var<int>("dut_var", "*.tb.*") > 4).build();
    uvm_expr* x2 = new expr(var<int>("dut_var", "*.tb.*") > 4);
    uvm_expr* x3 = to_expr(var<int>("dut_var", "*.tb.*") > 4);
  }

  void build_phase(uvm::uvm_phase& phase)
  {
    std::cout << sc_core::sc_time_stamp() << ": build_phase " << name() << std::endl;
    uvm_base_fault_env::build_phase(phase);


    /* --- Examples using the DSL --- */
    dsl_examples();
    /* --- Examples using the DSL --- */


    // uvm_expr* condition =  new uvm_gt(new uvm_var_tpl<int>("dut_var", "*.tb.*"), new uvm_int(4));
    // int a = -66;
    // uvm_fault* fault = new uvm_fault_set(new uvm_var_tpl<int>("dut_var", "*.tb.*"), &a, sizeof(int));
    // this->engine->register_fault_condition(condition, fault);


    // uvm_expr* condition =  new uvm_var_tpl< sc_core::sc_in<int> * >("dut_in", "*.tb.*");
    uvm_expr* condition =  new uvm_default_event<sc_core::sc_in<int>*>("dut_in", "*.tb.*");

    int a = -66;
    uvm_fault* fault = new uvm_fault_set(new uvm_var_tpl<int>("dut_var", "*.tb.*"), &a, sizeof(int));
    this->engine->register_fault_condition(condition, fault);

    // if (!uvm_config_db<sc_signal<int>*>::get(this, "*", "sg3", sg3))

  }

  UVM_COMPONENT_UTILS(vip_fault_env);

};

#endif /* VIP_FAULT_ENV_H_ */
