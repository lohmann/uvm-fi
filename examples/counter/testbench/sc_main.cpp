#include <systemc>
#include <uvm>

#include "vip_if.h"
#include "testbench.h"
#include "test.h"
#include <dut.h>
#include <time.h>

int sc_main(int, char*[])
{
  srand((unsigned) time(NULL));

  dut* my_dut = new dut("my_dut");

  sc_core::sc_clock clk("clk", 1, SC_NS);
  my_dut->counter_module.clock(clk);

  vip_if* dut_if_in = new vip_if();
  vip_if* dut_if_out = new vip_if();

  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vif_in", dut_if_in);
  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vif_out", dut_if_out);

  my_dut->counter_module.reset(dut_if_in->sig_reset);
  my_dut->counter_module.set(dut_if_in->sig_set);
  my_dut->counter_module.enable(dut_if_in->sig_enable);
  my_dut->counter_module.counter_out(dut_if_out->sig_counter_out);

  sc_core::sc_clock *a = &clk;
  uvm::uvm_config_db<sc_core::sc_clock*>::set(0, "*", "clock", a);

  sc_trace_file *fp;
  fp=sc_create_vcd_trace_file("wave");
  sc_trace(fp,clk,"clock");
  sc_trace(fp,my_dut->counter_module.reset,"reset");
  sc_trace(fp,my_dut->counter_module.enable,"enable");
  sc_trace(fp,my_dut->counter_module.counter_out,"output");
  
  sc_core::sc_in<int> *c = &(my_dut->counter_module.set);
  uvm::uvm_config_db<sc_core::sc_in<int> **>::set(0, "*", "input_event", &c);

  sc_dt::sc_uint<8> *b = &(my_dut->counter_module.count);
  uvm::uvm_config_db<sc_dt::sc_uint<8> **>::set(0, "*", "dut_count", &b);

  uvm::run_test("test");
  sc_close_vcd_trace_file(fp);
  return 0;
}
