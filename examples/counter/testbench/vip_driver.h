//----------------------------------------------------------------------
//   Copyright 2012-2014 NXP B.V.
//   All Rights Reserved Worldwide
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//----------------------------------------------------------------------

#ifndef VIP_DRIVER_H_
#define VIP_DRIVER_H_

#include <systemc>
#include <tlm.h>
#include <uvm>

#include <fi.h>

#include "vip_packet.h"
#include "vip_if.h"

template <class REQ>
class vip_driver : public base_driver<REQ>
{
 public:

  vip_if* vif_in;
  sc_core::sc_clock *clock;

  vip_driver( uvm::uvm_component_name name ) : base_driver<REQ>(name)
  {}

  UVM_COMPONENT_PARAM_UTILS(vip_driver<REQ>);

  void build_phase(uvm::uvm_phase& phase)
  {
    std::cout << sc_core::sc_time_stamp() << ": build_phase " << this->name() << std::endl;

    base_driver<REQ>::build_phase(phase);

    if (!uvm_config_db<vip_if*>::get(this, "*", "vif_in", vif_in))
      UVM_FATAL(this->name(), "Virtual interface not defined! Simulation aborted!");

    if (!uvm_config_db<sc_core::sc_clock*>::get(this, "*", "clock", clock))
      UVM_FATAL(this->name(), "Virtual interface not defined! Simulation aborted!");
  }

  virtual void pre_transfer(REQ& req) {
    this->seq_item_port->get_next_item(req);
  }

  virtual void drive_transfer(const REQ& p){
    sc_core::wait(clock->posedge_event());
    vif_in->sig_reset.write(p.reset);
    vif_in->sig_enable.write(p.enable);
    vif_in->sig_set.write(p.set);
  }

  virtual void post_transfer(REQ& req, REQ& rsp) {
    rsp.set_id_info(req);
    this->seq_item_port->item_done();
    this->seq_item_port->put_response(rsp);
  }
};

#endif /* VIP_DRIVER_H_ */
