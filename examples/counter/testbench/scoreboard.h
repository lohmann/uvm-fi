#ifndef SCOREBOARD_H_
#define SCOREBOARD_H_

#include <systemc>
#include <uvm>
#include <vector>

#include "xmt_subscriber.h"
#include "rcv_subscriber.h"
#include "vip_packet.h"

class scoreboard : public uvm::uvm_scoreboard
{
 public:
  uvm::uvm_analysis_export<vip_packet> xmt_listener_imp;
  uvm::uvm_analysis_export<vip_packet> rcv_listener_imp;

  xmt_subscriber* xmt_listener;
  rcv_subscriber* rcv_listener;

  std::vector<int> mistakes; // number of erros per matrix

  int counter = 0;
  bool result;

  scoreboard( uvm::uvm_component_name name ) : uvm::uvm_scoreboard( name )
  {
    std::cout << sc_core::sc_time_stamp() << ": constructor " << name << std::endl;
  }

  UVM_COMPONENT_UTILS(scoreboard);

  void golden_model(const vip_packet& p){
    //result = p.data; // just bypass the signal
  }  


  void write_xmt(const vip_packet& p)
  {
    std::cout << sc_core::sc_time_stamp() << ": " << name() << " xmt_listener in scoreboard "
        "received value: " << p.reset << " ::: " << p.enable <<  std::endl;
      if (p.reset == 1) {
        counter = 0; 
      } else if (p.enable == 1) {
        counter = counter + 1;
      }

  }

  void write_rcv(const vip_packet& p)
  {
    std::cout << sc_core::sc_time_stamp() << ": " << name() << " rcv_listener in scoreboard "
        "received value: " << p.counter_out << std::endl;

    mistakes.push_back(0);
      if(p.counter_out != counter) {
            std::cout << " ERROUR FAULTTTTT" << std::endl;
        mistakes.back()++; //= mistakes.at(i)+1; 
      }
  }

  void build_phase(uvm::uvm_phase& phase)
  {
    uvm::uvm_scoreboard::build_phase(phase);
    std::cout << sc_core::sc_time_stamp() << ": build_phase " << name() << std::endl;

    uvm::uvm_config_db<uvm_object*>::set(this, "xmt_listener", "sb", this);
    uvm::uvm_config_db<uvm_object*>::set(this, "rcv_listener", "sb", this);

    xmt_listener = xmt_subscriber::type_id::create("xmt_listener", this);
    assert(xmt_listener);

    rcv_listener = rcv_subscriber::type_id::create("rcv_listener", this);
    assert(rcv_listener);
  }

  void connect_phase(uvm::uvm_phase& phase)
  {
    xmt_listener_imp.connect(xmt_listener->analysis_export);
    rcv_listener_imp.connect(rcv_listener->analysis_export);
  }

};

#endif /* SCOREBOARD_H_ */
