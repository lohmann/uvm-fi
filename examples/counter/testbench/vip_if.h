#ifndef VIP_IF_H_
#define VIP_IF_H_

#include <systemc>
#include <uvm>

using namespace uvm;

using namespace sc_core;
using namespace sc_dt;

class vip_if
{
 public:
  sc_signal<int> sig_set;
  sc_signal<bool> sig_reset;
  sc_signal<bool> sig_enable;
  sc_signal<sc_uint<8>> sig_counter_out;

  vip_if() {}
};


#endif /* VIP_IF_H_ */
