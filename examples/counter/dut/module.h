#ifndef MODULE_H_
#define MODULE_H_

#include <systemc>

using namespace uvm_dsl;
using namespace sc_core;
using namespace sc_dt;

SC_MODULE (counter8bit) {
  sc_in_clk clock ;
  sc_in<bool> reset, enable;
  sc_out<sc_uint<8> > counter_out;
  sc_uint<8> count;
  sc_in<int> set;
  int count_fake = 0;

  void incr_count() {

    while(true) {
      if (reset.read() == 1 || (count_fake >= 3 && count_fake <= 6)) {
        count = 0; counter_out.write(count);
        count_fake++;
      } else if (enable.read() == 1) {
        count = count + 1;
        counter_out.write(count);
        count_fake++;
      }
      std::cout << " **** Count : " << count << std::endl;
      std::cout << " **** Reset : " << reset << std::endl;
      std::cout << " **** Enable : " << enable << std::endl;
      wait();
    }
  }

  SC_CTOR(counter8bit) {
    SC_THREAD(incr_count);
    sensitive << reset << clock.pos();
  }
};

#endif /* MODULE_H_ */

