#ifndef DUT_H_
#define DUT_H_

#include <systemc>

#include "module.h"

class dut : public sc_core::sc_module {
 public:
  counter8bit counter_module;

  SC_CTOR(dut) : counter_module("counter_module") 
  {
  }
};

#endif /* DUT_H_ */
