#ifndef DUT_H_
#define DUT_H_

#include <systemc.h>

#include "matrix_mult.h"
#include "voter.h"
#include "printer.h"


SC_MODULE(dut) {

  sc_core::sc_in<sc_dt::sc_int<16> > A[16];
  sc_core::sc_in<sc_dt::sc_int<16> > B[16];

  sc_core::sc_port<sc_core::sc_signal_inout_if<sc_dt::sc_int<16> > , 2> C[16];


  sc_core::sc_signal< sc_dt::sc_int<16> > sg1[16];
  sc_core::sc_signal< sc_dt::sc_int<16> > sg2[16];
  sc_core::sc_signal< sc_dt::sc_int<16> > sg3[16];
  sc_core::sc_signal<sc_dt::sc_int<16>> signal_printer[16];

  // Modules
  matrix_mult m1;
  matrix_mult m2;
  matrix_mult m3;
  voter vt;

  Printer printer_;

  SC_CTOR(dut) : m1("m1"), m2("m2"), m3("m3"), vt("vt"), printer_("printer") {

    int z;
    for (z=0; z<16; z++) {

        m1.A[z](A[z]);
        m1.B[z](B[z]);
        m1.C[z](sg1[z]);

        m2.A[z](A[z]);
        m2.B[z](B[z]);
        m2.C[z](sg2[z]);

        m3.A[z](A[z]);
        m3.B[z](B[z]);
        m3.C[z](sg3[z]);


        vt.input_A[z](sg1[z]);
        vt.input_B[z](sg2[z]);
        vt.input_C[z](sg3[z]);

        vt.output[z](signal_printer[z]);

        printer_.input_a[z](signal_printer[z]);
        printer_.output_a[z](C[z]);
    }
  }

};

#endif /* DUT_H_ */
