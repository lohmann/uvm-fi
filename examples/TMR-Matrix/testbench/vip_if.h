#ifndef VIP_IF_H_
#define VIP_IF_H_

#include <systemc>
#include <uvm>

using namespace uvm;

class vip_if
{
 public:
  sc_core::sc_signal< sc_dt::sc_int<16> > sig_data[16];

  vip_if() {}
};


#endif /* VIP_IF_H_ */
