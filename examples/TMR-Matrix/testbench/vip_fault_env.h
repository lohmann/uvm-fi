//----------------------------------------------------------------------
//   Copyright 2012-2014 NXP B.V.
//   All Rights Reserved Worldwide
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//----------------------------------------------------------------------

#ifndef VIP_FAULT_ENV_H_
#define VIP_FAULT_ENV_H_

#include <systemc>
#include <tlm.h>
#include <uvm>

#include <fi.h>

int really_complicated_function(int x, int y) {
  return x + y;
}

template<class T> T mov_avg(std::deque<T>* win, int win_size, T value) {
  typedef typename std::deque<T>::const_iterator it_t;
  if (win->size() >= win_size) win->pop_front();
  win->push_back(value);
  T sum = 0;
  for (it_t i = win->begin(), e = win->end(); i != e; ++i)
     sum += *i;
  return (T)(sum/(double)win_size);
}

class vip_fault_env : public uvm_base_fault_env
{
 public:
  sc_dt::sc_int<16> a;
  sc_dt::sc_int<16> b;
  sc_dt::sc_int<16> c;
  std::deque<sc_dt::sc_int<32>> hist;
  int one, two;

  vip_fault_env( uvm::uvm_component_name name ) : uvm_base_fault_env( name ){
    a = 666;
    b = 667;
    c = 668;
    one = 1;
    two = 2;
  }

  void build_phase(uvm::uvm_phase& phase)
  {
    using namespace uvm_dsl;
    using namespace sc_core;
    using namespace sc_dt;

    std::cout << sc_core::sc_time_stamp() << ": build_phase " << name() << std::endl;
    uvm_base_fault_env::build_phase(phase);
    
//    uvm_expr  * condition = new uvm_default_event<sc_out<sc_int<16> > *>("m1_c_out_13","*");
//    uvm_fault * fault = new uvm_fault_set(new uvm_var_tpl<sc_signal<sc_int<16> >*>("signal_m3_vt", "*"), &c, sizeof(sc_int<16> ));
//    this->engine->register_fault_condition(condition,fault);

//    *engine << fm(cnst(true), var<char>("tgt").set('\xff'));
//    *engine << fm(unif(1,10) <= 9, var<char>("tgt").set('\xff'));
//    bool truth = true;
//    engine->register_fault_condition(new uvm_var_ct_tpl<bool>(true),
//        new uvm_fault_set(new uvm_var_tpl<bool>("tgt", "*"), &truth, 0, 1));
//
//
//    *engine << fm(evt<sc_signal<sc_int<16>>*>("m_out1"),
//        var<sc_int<32>>("tgt").set(call(&mov_avg<sc_int<32>>,
//            cnst(&hist), cnst(3), var<sc_int<32>>("m_out1"))));

      int* v = (int*)call(&really_complicated_function, cap(one), cap(two)).build()->get();
      std::cout << "test1: " << (*v == 3) << ", *v==" << *v << std::endl;
      free(v);

      evt<sc_out<sc_int<16> > *> m1_c_out_13("m1_c_out_13","*");
	    *engine << fm(m1_c_out_13 &&  (33 > unif(0, 100)),
	                  var<sc_signal<sc_int<16> >*>("signal_m1_vt", "*").set(cap(a)));
	    *engine << fm(evt<sc_out<sc_int<16> > *>("m1_c_out_14","*") && (33 > unif(0, 100)),
	                  var<sc_signal<sc_int<16> >*>("signal_m2_vt", "*").set(cap(b)));
	    *engine << fm(evt<sc_out<sc_int<16> > *>("m1_c_out_15","*") && (33 > unif(0, 100)),
	                  var<sc_signal<sc_int<16> >*>("signal_m3_vt", "*").set(cap(c)));
  }

  UVM_COMPONENT_UTILS(vip_fault_env);

};

#endif /* VIP_FAULT_ENV_H_ */
