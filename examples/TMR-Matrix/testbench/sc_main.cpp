#include <systemc>
#include <uvm>

#include "vip_if.h"
#include "testbench.h"
#include "test.h"
#include <dut.h>
#include <time.h>       /* time */

int sc_main(int, char*[])
{
  srand((unsigned) time(NULL));
  // instantiate the DUT
  dut* my_dut = new dut("my_dut");

  // uvm_config_db_options::turn_on_tracing();

  vip_if* dut_if_in_A = new vip_if();
  vip_if* dut_if_in_B = new vip_if();
  vip_if* dut_if_out = new vip_if();

  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vifA", dut_if_in_A);
  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vifB", dut_if_in_B);
  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vifOut", dut_if_out);

  int z;
  for (z=0; z<16; z++) {
      my_dut->A[z](dut_if_in_A->sig_data[z]);
      my_dut->B[z](dut_if_in_B->sig_data[z]);
      my_dut->C[z](dut_if_out->sig_data[z]);
  }

  // control the fault injection - trigger event
  sc_core::sc_out<sc_dt::sc_int<16> > * a = &(my_dut->m1.C[13]);
  uvm::uvm_config_db<sc_core::sc_out<sc_dt::sc_int<16> > **>::set(0, "*", "m1_c_out_13", &a);

  sc_core::sc_out<sc_dt::sc_int<16> > * b = &(my_dut->m1.C[14]);
  uvm::uvm_config_db<sc_core::sc_out<sc_dt::sc_int<16> > **>::set(0, "*", "m1_c_out_14", &b);

  sc_core::sc_out<sc_dt::sc_int<16> > * c = &(my_dut->m1.C[15]);
  uvm::uvm_config_db<sc_core::sc_out<sc_dt::sc_int<16> > **>::set(0, "*", "m1_c_out_15", &c);

  // locale that faults will be injected
  sc_core::sc_signal<sc_dt::sc_int<16> > * d = &(my_dut->sg1[12]);
  uvm::uvm_config_db<sc_core::sc_signal< sc_dt::sc_int<16> > **>::set(0, "*","signal_m1_vt", &d);

  sc_core::sc_signal<sc_dt::sc_int<16> > * e = &(my_dut->sg2[12]);
  uvm::uvm_config_db<sc_core::sc_signal< sc_dt::sc_int<16> > **>::set(0, "*","signal_m2_vt", &e);

  sc_core::sc_signal<sc_dt::sc_int<16> > * f = &(my_dut->sg3[12]);
  uvm::uvm_config_db<sc_core::sc_signal< sc_dt::sc_int<16> > **>::set(0, "*","signal_m3_vt", &f);

  // uvm::uvm_config_db<sc_core::sc_out<sc_dt::sc_int<16> > **>::dump();

  uvm::run_test("test");

  return 0;
}
