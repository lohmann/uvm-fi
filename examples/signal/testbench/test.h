#include <uvm>
#include <systemc>
#include <string>
#include <iostream>

#include "testbench.h"

#include "sequence.h"

#include "vip_if.h"

class test: public uvm::uvm_test
{
public:
  testbench* tb;
  bool test_pass;

  test(uvm::uvm_component_name name)
  : uvm_test(name),
    tb(0),
    test_pass(false)
  {}

  UVM_COMPONENT_UTILS(test);

  virtual void build_phase(uvm::uvm_phase& phase )
    {
      uvm::uvm_test::build_phase(phase);

      tb = testbench::type_id::create("tb", this);
      assert(tb);

      uvm::uvm_config_db<uvm_object_wrapper*>
        ::set(this,"tb.agent_A.sequencer.run_phase","default_sequence",
        sequence<vip_packet>::type_id::get());
    }

    virtual void run_phase( uvm::uvm_phase& phase )
    {
      UVM_INFO( get_name(), "** UVM TEST STARTED **", UVM_NONE );
    }

    virtual void extract_phase( uvm::uvm_phase& phase )
    {
      std::cout << sc_core::sc_time_stamp() << " extract_phase  " << std::endl;
    }

    virtual void report_phase( uvm::uvm_phase& phase )
    {
      std::cout << sc_core::sc_time_stamp() << " extract_phase  " << std::endl;

      int failures_number = 0;
      for(int i = 0; i < tb->scoreboard_test->mistakes.size(); i++){
        std::cout << "Test number "<< i << " has " << tb->scoreboard_test->mistakes.at(i) << " wrong elements" << std::endl;
        if(tb->scoreboard_test->mistakes.at(i) > 0){
          failures_number++;
        }
      }
      std::cout << "Total results for "<< tb->scoreboard_test->mistakes.size() << " simulations: "  << std::endl;
      std::cout << "Number of faults injected : "<< x << std::endl;
      std::cout << "Number of failures : "<< failures_number << std::endl;
      uvm::uvm_root::get()->print_topology();
    }
};
