#include <systemc>
#include <uvm>

#include "vip_if.h"
#include "testbench.h"
#include "test.h"
#include <dut.h>
#include <time.h>

int sc_main(int, char*[])
{
  srand((unsigned) time(NULL));

  dut* my_dut = new dut("my_dut");

  vip_if* dut_if_in = new vip_if();
  vip_if* dut_if_out = new vip_if();

  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vif_in", dut_if_in);
  uvm::uvm_config_db<vip_if*>::set(0, "*.tb.*", "vif_out", dut_if_out);

  my_dut->module_a.in(dut_if_in->sig_data);
  my_dut->module_b.out(dut_if_out->sig_data);

  sc_core::sc_in<bool> *a = &(my_dut->module_a.in);
  uvm::uvm_config_db<sc_core::sc_in<bool> **>::set(0, "*", "input_event", &a);

  sc_core::sc_signal<bool> *b = &(my_dut->signal_connection);
  uvm::uvm_config_db<sc_core::sc_signal<bool> **>::set(0, "*", "dut_internal_signal", &b);

  uvm::run_test("test");
  return 0;
}
