#ifndef VIP_PACKET_H
#define VIP_PACKET_H

#include <systemc>
#include <tlm.h>
#include <uvm>
#include <vector>

/////////////////

class vip_packet : public uvm::uvm_sequence_item
{
 public:
  UVM_OBJECT_UTILS(vip_packet);

  vip_packet(const std::string& name = "packet") { }
  virtual ~vip_packet() { }

 public:
  bool data;
};

#endif /* VIP_PACKET_H_ */
