//----------------------------------------------------------------------
//   Copyright 2012-2014 NXP B.V.
//   All Rights Reserved Worldwide
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//----------------------------------------------------------------------

#ifndef VIP_FAULT_ENV_H_
#define VIP_FAULT_ENV_H_

#include <systemc>
#include <tlm.h>
#include <uvm>

#include <fi.h>

int really_complicated_function(int x, int y) {
  return x + y;
}

template<class T> T mov_avg(std::deque<T>* win, int win_size, T value) {
  typedef typename std::deque<T>::const_iterator it_t;
  if (win->size() >= win_size) win->pop_front();
  win->push_back(value);
  T sum = 0;
  for (it_t i = win->begin(), e = win->end(); i != e; ++i)
     sum += *i;
  return (T)(sum/(double)win_size);
}

class vip_fault_env : public uvm_base_fault_env
{
 public:
  bool a;

  vip_fault_env( uvm::uvm_component_name name ) : uvm_base_fault_env( name ) {
    a = false;
  }

  void build_phase(uvm::uvm_phase& phase)
  {
    using namespace uvm_dsl;
    using namespace sc_core;
    using namespace sc_dt;
    std::cout << sc_core::sc_time_stamp() << ": build_phase " << name() << std::endl;
    uvm_base_fault_env::build_phase(phase);

    evt<sc_in<bool>*> input_evt("input_event","*");
    *engine << fm(input_evt,
                  var<sc_signal<bool>*>("dut_internal_signal", "*").set(cap(a)));
  }

  UVM_COMPONENT_UTILS(vip_fault_env);

};

#endif /* VIP_FAULT_ENV_H_ */
