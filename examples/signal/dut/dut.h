#ifndef DUT_H_
#define DUT_H_

#include <systemc>

#include "module.h"

class dut : public sc_core::sc_module {
 public:
  sc_core::sc_signal<bool> signal_connection;

  module module_a;
  module module_b;

  SC_CTOR(dut) : module_a("module_a"), module_b("module_b")
  {
    module_a.out(signal_connection);
    module_b.in(signal_connection);
  }
};

#endif /* DUT_H_ */
