#ifndef MODULE_H_
#define MODULE_H_

#include <systemc>

class module : public sc_core::sc_module
{
 public:
  sc_core::sc_in<bool> in;
  sc_core::sc_out<bool> out;

  void func();

  SC_CTOR(module) : in("in"), out("out")
  {
    SC_METHOD(func);
    sensitive << in;
    dont_initialize();
  }

};

#endif /* MODULE_H_ */
