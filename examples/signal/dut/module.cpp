#include "module.h"

void module::func() {
  bool val;
  val = in.read();
  std::cout << sc_core::sc_time_stamp() << ": " << name() << " received value "
      << val << std::endl;
  std::cout << sc_core::sc_time_stamp() << ": " << name() << " send value "
      << val << std::endl;
  out.write(val);
}
